import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.hamcrest.Matchers;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Ignore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.TimeUnit;


//import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath();

public class ApiTest  {
    OAuth2Filter oAuth2Filter = new OAuth2Filter("1234");

    JSONParser parser = new JSONParser();
    Object obj = parser.parse(new FileReader("src/test/java/createMapping.json"));
    JSONObject jsonObject = (JSONObject) obj;

    public ApiTest() throws IOException, ParseException {
    }
    RequestSpecification requestMappings = new RequestSpecBuilder()
            .setBaseUri("http://wiremock.atlantis.t-systems.ru/")
            .addFilter(new ResponseLoggingFilter())
            .addFilter(new RequestLoggingFilter())
            .addFilter(oAuth2Filter)
            .build();

    ResponseSpecification responceMappings = new ResponseSpecBuilder()
            .expectHeader("Content-Type", "application/json")
            .expectResponseTime(Matchers.lessThan(2L), TimeUnit.SECONDS)
            .build();


    @BeforeEach
    public void addSpecifications() {
        RestAssured.requestSpecification = requestMappings;
        RestAssured.responseSpecification = responceMappings;
    }

    @Test
    public void apiTest() {
        RestAssured.given()
                .when().post("/exam/mapping")
                .then().assertThat()
                .statusCode(202)
                .body("$", Matchers.hasEntry("postcheck", "success"));
//                .extract().path("id");
    }

    @Test
    public void checkMyMapping() {
        RestAssured.given()
                .when().get("/tau/albizhum")
                .then()
                .statusCode(200)
                .body("$", Matchers.hasEntry("searchResults", "42"));
    }

    @Ignore
    @Nested
    class createMapping {

        String id;
        @Test
        public void createMyMapping() {
            id = RestAssured.given().spec(requestMappings.body(jsonObject))
                    .when().post("/__admin/mappings")
                    .then()
                    .assertThat()
                    .statusCode(201)
//                .body("id")
//                .body("id", equalTo(testMovie.getId()))
                    .extract().path("id");
//                .body("$", Matchers.hasEntry("jsonBody", "{searchResults=42}"));
            System.out.println(id);
        }


        public void deleteMyMapping() {
            RestAssured.given().spec(requestMappings.body(jsonObject))
                    .when().delete("/__admin/mappings")
                    .then()
                    .assertThat()
                    .statusCode(201)
                    .body("$", Matchers.hasEntry("jsonBody", "{searchResults=42}"));
        }

    }






}
