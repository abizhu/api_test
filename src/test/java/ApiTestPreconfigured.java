import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.hamcrest.Matchers;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Ignore;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static io.restassured.RestAssured.given;


//import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath();

public class ApiTestPreconfigured {
    OAuth2Filter oAuth2Filter = new OAuth2Filter("1234");

    @BeforeAll
    public static void configureRestAssured() {
        RestAssured.baseURI = "http://wiremock.atlantis.t-systems.ru/";
        RestAssured.requestSpecification = given()
                .header("Connection", "keep-alive");
        RestAssured.filters(new RequestLoggingFilter(), new ResponseLoggingFilter());
    }


    ResponseSpecification responceSucessMappings = new ResponseSpecBuilder()
            .expectStatusCode(200)
            .build();

    @Ignore
    @Test
    public void checkMyMapping() {
        given().when().get("/tau/albizhum")
                .then()
                .time(Matchers.lessThan(1L), TimeUnit.SECONDS)
                .spec(responceSucessMappings)
                .body("$", Matchers.hasEntry("searchResults", "42"));
    }

}
